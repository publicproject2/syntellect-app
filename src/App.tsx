import React from "react";
import "./App.css";
import { SectionControlWithButtons } from "./components/SectionControlWithButtons";
import { SectionsWithAutocomplete } from "./components/SectionsWithAutocomplete";

function App() {
  return (
    <div>
      <h3>Тест первый контрол</h3>
      <SectionControlWithButtons />
      <br />
      <h3>Тест второй контрол</h3>
      <SectionsWithAutocomplete />
    </div>
  );
}

export default App;
