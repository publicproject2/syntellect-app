import { useState } from "react";
import {
  ControlWithButtons,
  IControlWithButtonsProps,
} from "./ControlWithButtons";

const useControlWithTwoRightButtons = (): IControlWithButtonsProps => {
  const [controlValue, setControlValue] = useState("");

  const handleControlButtonClick = (buttonIndex: number) => {
    if (buttonIndex === 0) {
      setControlValue("");
    } else if (buttonIndex === 1) {
      setControlValue("Hello world!");
    }
  };

  return {
    rightButtons: [
      { text: "Clear", onClick: () => handleControlButtonClick(0) },
      { text: "Hello", onClick: () => handleControlButtonClick(1) },
    ],
    value: controlValue,
    onChange: setControlValue,
  };
};

const useControlWithLeftRightButtons = (): IControlWithButtonsProps => {
  const [controlValue, setControlValue] = useState("");

  const handleControlButtonClick = (buttonIndex: number) => {
    if (buttonIndex === 0) {
      alert(controlValue);
    } else if (buttonIndex === 1) {
      const numberValue = parseFloat(controlValue);
      if (!isNaN(numberValue)) {
        alert(numberValue);
      } else {
        alert("Please enter valid number");
      }
    }
  };

  return {
    leftButtons: [{ text: "Show", onClick: () => handleControlButtonClick(1) }],
    rightButtons: [
      { text: "Alert", onClick: () => handleControlButtonClick(0) },
    ],
    value: controlValue,
    onChange: setControlValue,
  };
};

export const SectionControlWithButtons = () => {
  const controlWithTwoRightButtonsProps = useControlWithTwoRightButtons();
  const controlWithLeftRightButtonsProps = useControlWithLeftRightButtons();

  return (
    <>
      <ControlWithButtons {...controlWithTwoRightButtonsProps} />
      <ControlWithButtons {...controlWithLeftRightButtonsProps} />
    </>
  );
};
