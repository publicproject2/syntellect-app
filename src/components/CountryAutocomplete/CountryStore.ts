import { makeAutoObservable } from "mobx";
import { CountryInfo, getCountryByName } from "../../api/apiService";

export class CountryStore {
  suggestions: CountryInfo[] = [];
  maxSuggestions: number = 3;
  cancel = false;

  constructor() {
    makeAutoObservable(this);
  }

  setMaxSuggestions(max: number) {
    this.maxSuggestions = max;
  }

  clearSuggestions() {
    this.cancel = true;
    this.setSuggestions([]);
  }

  async fetchSuggestions(countryName: string) {
    try {
      this.cancel = false;
      const suggestions = await getCountryByName(countryName);
      if (!this.cancel) {
        this.setSuggestions(suggestions.slice(0, this.maxSuggestions));
      }
    } catch (error) {
      console.error("Error fetching country suggestions:", error);
    }
  }

  setSuggestions(suggestions: CountryInfo[]) {
    const uniqueSuggestions = suggestions.filter(
      (suggestion) =>
        !this.suggestions.some(
          (existingSuggestion) => existingSuggestion.name === suggestion.name
        )
    );
    this.suggestions = uniqueSuggestions.slice(0, this.maxSuggestions);
  }
}
