import { useRef } from "react";
import { CountryStore } from "../CountryStore";

export const useCountryStore = (maxSuggestions: number) => {
  const storeRef = useRef<CountryStore | null>(null);

  if (!storeRef.current) {
    storeRef.current = new CountryStore();
    storeRef.current.setMaxSuggestions(maxSuggestions);
  }

  return storeRef.current;
};
