import React, { useState, useEffect, useRef } from "react";
import { CountryInfo } from "../../api/apiService";
import { observer } from "mobx-react-lite";
import { useCountryStore } from "./hooks/useCountryStore";
import { useDebouncedValue } from "./hooks/useDebouncedValue ";
import { ITextControlWithSuggestionsProps } from "./types";

interface IAutoComplete {
  inputValue: string;
  handleChange: (value: string) => void;
  handleSuggestionClick: (country: CountryInfo) => void;
  suggestions: CountryInfo[];
  ref: React.RefObject<HTMLInputElement>;
}

const useAutocomplete = (maxSuggestions: number): IAutoComplete => {
  const [inputValue, setInputValue] = useState("");
  const [choosenValue, setChoosenValue] = useState("");

  const ref = useRef<HTMLInputElement>(null);

  const store = useCountryStore(maxSuggestions);

  const debouncedSearchString = useDebouncedValue(inputValue, 300);

  useEffect(() => {
    const inputElement = ref.current;
    if (!inputElement) {
      return;
    }

    const onLostFocus = () => {
      setInputValue("");
    };

    inputElement.addEventListener("blur", onLostFocus);

    return () => {
      inputElement?.removeEventListener("blur", onLostFocus);
    };
  }, [store]);

  useEffect(() => {
    if (inputValue === "") {
      store.clearSuggestions();
    }
  }, [inputValue, store]);

  useEffect(() => {
    if (
      choosenValue !== debouncedSearchString &&
      debouncedSearchString !== ""
    ) {
      store.fetchSuggestions(debouncedSearchString);
    }
  }, [choosenValue, debouncedSearchString, store]);

  const handleChange = (value: string) => {
    setInputValue(value);
  };

  const handleSuggestionClick = (country: CountryInfo) => {
    setChoosenValue(country.name);
    setInputValue(country.name);
    store.setSuggestions([]);
  };

  return {
    inputValue,
    handleChange,
    handleSuggestionClick,
    suggestions: store.suggestions,
    ref,
  };
};

const CountryAutocompleteImpl: React.FC<ITextControlWithSuggestionsProps> = ({
  maxSuggestions,
}) => {
  const { inputValue, handleChange, handleSuggestionClick, suggestions, ref } =
    useAutocomplete(maxSuggestions);

  return (
    <div>
      <input
        type="text"
        value={inputValue}
        onChange={(e) => handleChange(e.target.value)}
        ref={ref}
      />

      <ul>
        {suggestions.map((country, index) => (
          <li key={index} onClick={() => handleSuggestionClick(country)}>
            <p>
              <img src={country.flag} alt="flag" /> ({country.name} -{" "}
              {country.fullName})
            </p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export const CountryAutocomplete = observer(CountryAutocompleteImpl);
