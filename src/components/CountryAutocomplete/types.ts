export interface ITextControlWithSuggestionsProps {
  maxSuggestions: number;
}
