import React from "react";
import { IButtonProps, IControlWithButtonsProps } from "./types";

const Button: React.FC<IButtonProps> = ({ text, onClick }) => {
  return <button onClick={onClick}>{text}</button>;
};

export const ControlWithButtons: React.FC<IControlWithButtonsProps> = ({
  leftButtons = [],
  rightButtons = [],
  value,
  onChange,
}) => {
  return (
    <div>
      {leftButtons.map((button, index) => (
        <Button
          key={`left-${index}`}
          text={button.text}
          onClick={button.onClick}
        />
      ))}
      <input
        type="text"
        value={value}
        onChange={(e) => onChange(e.target.value)}
      />
      {rightButtons.map((button, index) => (
        <Button
          key={`right-${index}`}
          text={button.text}
          onClick={button.onClick}
        />
      ))}
    </div>
  );
};
