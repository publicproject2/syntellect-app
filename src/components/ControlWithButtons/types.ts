export interface IButtonProps {
  text: string;
  onClick: () => void;
}

export interface IControlWithButtonsProps {
  leftButtons?: IButtonProps[];
  rightButtons?: IButtonProps[];
  value: string;
  onChange: (newValue: string) => void;
}
