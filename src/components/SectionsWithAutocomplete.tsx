import { CountryAutocomplete } from "./CountryAutocomplete";

export const SectionsWithAutocomplete = () => {
  return (
    <>
      <CountryAutocomplete maxSuggestions={3} />

      <CountryAutocomplete maxSuggestions={10} />
    </>
  );
};
